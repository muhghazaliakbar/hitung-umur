package com.skm.muhghazaliakbar.hitungumur;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    /**
     * Define variable.
     */
    EditText input;
    Button submit;
    TextView result;

    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    int age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Set defined variable into component.
         */
        input = (EditText) findViewById(R.id.input);
        submit = (Button) findViewById(R.id.submit);
        result = (TextView) findViewById(R.id.result);

        /**
         * Create on click event to submit button to count the age.
         */
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * Check log input and current year.
                 *
                 * - [Not important]
                 */
                Log.v("Result Input", input.getText().toString());
                Log.v("Current Year", String.valueOf(currentYear));

                /**
                 * Count age.
                 *
                 * - Age = Current year - Input Year of Birth.
                 */
                age = currentYear - Integer.parseInt(input.getText().toString());

                /**
                 * Show result into Text View.
                 */
                result.setText(age + " Tahun");
            }
        });
    }
}
